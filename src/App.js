// import css
import './App.css'
// use library of state on wed
import { useState } from "react";
// use AnimalShow
import AnimalShow from "./AnimalShow";


function getRandomAnimal() {
    const animals = ['bird', 'cat', 'cow', 'dog', 'gator', 'horse'];
     return animals[Math.floor(Math.random() * animals.length)]
}
// console.log(getRandomAnimal())

function App() {
    const [animals, setAnimals] = useState([])

    const handleClick = () => {
        // modifies a peice of state!!
        setAnimals([...animals, getRandomAnimal()])
    }
    const renderedAnimals = animals.map((animal, index) => {
        return <AnimalShow type={animal} key={index}/>
    })
    return (
        <div className='app'>
            <button onClick={handleClick}>Add Animal</button>     
            <div className='animal-list'>{renderedAnimals}</div>
        </div>

    )
}
export default App;